namespace BlazorTest.Models; 

public class TodoItem {
    
    public string title { get; set; }
    public bool isDone { get; set; }
    
}